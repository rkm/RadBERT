---
widget:
- text: "low lung volumes, [MASK] pulmonary vascularity."
tags:
- fill-mask
- pytorch
- transformers
- bert
- biobert 
- radbert 
- language-model
- uncased
- radiology
- biomedical
datasets:
- wikipedia
- bookscorpus
- pubmed
- radreports
language:
  - en
license: mit
---

RadBERT was continuously pre-trained on radiology reports from a BioBERT initialization. Manuscript in proceedings.